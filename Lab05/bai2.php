<?php
    class Counter{
        private static $counter = 0;
        const VERSION = 2.0;

        function __construct(){
            self::$counter++;
        }

        function __destruct(){
            self::$counter--;
        }

        public function getCounter(){
            return self::$counter;
        }
    }

    $obj = new Counter();
    print $obj->getCounter().'<br>';
    $obj2 = new Counter();
    print $obj2->getCounter().'<br>';
    $obj2 = NULL;
    print $obj->getCounter().'<br>';
    print 'Version use : '.Counter::VERSION.'<br>';
?>