<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

    <form action="" method="post">
        <h1>Happy Harry's Hardware Product Information</h1>
        <table>
            <tr>
                <td>Enter product code: </td>
                <td><input type="text", size="6" name="code"></td>
            </tr>

            <tr>
                <td>Please enter description: </td>
                <td><input type="text", size="50" name="description"></td>
            </tr>

            <tr>
                <td align="right"><input type="submit" name="submit" value="Check validation"></td>
                <td align="light"><input type="reset" value="Reset"></td>
            </tr>
        </table>

    </form>

    <?php
        if(isset($_POST['submit'])){
            $description = $_POST["description"];
            $code = $_POST["code"];
            $products = array('AB01'=>'25-Pound Sledgehammer',
                            'AB02'=>'Extra Strong Nails',
                            'AB03'=>'Super Adjustable Wrench',
                            'AB04'=>'3-Speed Electric Screwdriver'
            );

            if (mb_ereg("boat|plane", $description)){
                print "Sorry,we do not sell boats or planes anymore";
            } elseif(mb_ereg('^AB', $code)) {
                if (isset($products["$code"])){
                    print "Code $code Description: $products[$code]";
                }else{
                    print "Sorry, product code not found";
                }
            }else{
                print "Sorry, all our product codes start with \"AB\"";
            }
        }
    ?>
</body>
</html>