<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lab03 bai1</title>
</head>
<body>

    <?php 
        function printOption($batdau,$ketthuc,$name) {  
            $select = -1;
            if(isset($_POST[$name])) {
                $select = $_POST[$name];
            }  
            if(isset($_POST['reset'])) {
                $select = -1;
            }
            for($i=$batdau ; $i<=$ketthuc ; $i++){
                if($i == $select){
                    print "<option value=".$i." selected>".$i."</option>";
                }else{
                    print "<option value=".$i.">".$i."</option>";
                }             
            }
        }

        function printDate($date,$month,$year) {  
            $select = -1;
            if(isset($_POST['submit'])){
                if(isset($_POST[$date])) {
                    $select = $_POST[$date];
                }  
                switch($_POST[$month]){
                    case 1:
                    case 3:
                    case 5:
                    case 7:
                    case 8:
                    case 10:
                    case 12:
                        $ketthuc = 31;
                        break;
                    case 2:
                        if($_POST[$year]%4 == 0){
                            if($_POST[$year]%100 == 0 && $_POST[$year]%400 == 0){
                                $ketthuc = 29;
                            }else{
                                $ketthuc = 28;
                            }
                        }else{
                            $ketthuc = 28;
                        }
                        break;
                    case 4:
                    case 6:
                    case 9:
                    case 11:
                        $ketthuc = 30;
                        break;
                    default:
                        break;
                }
                for($i=0 ; $i<=$ketthuc ; $i++){
                    if($i == $select){
                        print "<option value=".$i." selected>".$i."</option>";
                    }else{
                        print "<option value=".$i.">".$i."</option>";
                    }             
                }
            }
        }
    ?>

    <form action="" method="post">
        <h1>Enter your name,date and time</h1>
        <span>Your name</span>
        <input type="text" name="name" placeholder="Enter your name" value="<?php if(isset($_POST['submit'])){if(isset($_POST["name"])){echo $_POST["name"];}} ?>" />
        <p>Date</p>
        <span>Date</span>
        <select id="date" <?php if(!isset($_POST['submit'])){ echo 'disabled';} ?> name="date">
            <option value="0">Chọn ngày</option>
            <?php printDate('date','month','year') ?>
        </select>
        <span>Month</span>
        <select <?php if(!isset($_POST['submit'])){ echo 'disabled';} ?> id="month" name="month">
            <option value="0">Chọn tháng</option>
            <?php printOption(1,12,'month'); ?>
        </select>
        <span>year</span>
        <select id="year" name="year">
            <option value="0">Chọn năm</option>
            <?php printOption(1990,2030,'year'); ?>
        </select>
        <p>Time</p>
        <span>hour<span>
        <select name="hour">
            <option value="0">Chọn giờ</option>
            <?php printOption(1,24,'hour'); ?>
        </select>
        <span>minute</span>
        <select name="minute">
            <option value="0">Chọn phút</option>
            <?php printOption(0,59,'minute'); ?>
        </select>
        <span>second</span>
        <select name="second">
            <option value="0">Chọn giây</option>
            <?php printOption(0,59,'second'); ?>
        </select>
        <p></p>
        <div>
            <button type="submit" name="submit">Submit</button>
            <button type="submit" name="reset">Reset</button>
        </div>
        <p></p>
    </form>

    <?php
        if(isset($_POST['submit'])){
            if(isset($_POST['name'])){
                print 'Hi '.$_POST['name'].' !'.'</br>';
            }
            if(isset($_POST['month']) && isset($_POST['year']) && isset($_POST['hour']) && isset($_POST['minute']) && isset($_POST['second'])){
                print 'You have choose to have an appointment on '.$_POST['hour'].':'.$_POST['minute'].':'.$_POST['second'].' , '.$_POST['date'].'/'.$_POST['month'].'/'.$_POST['year'].'<br>';
            }
            echo "<br>More information <br><br>";
            print 'In 12 hours, the time and date is ';
            if($_POST['hour'] > 12){
                print ($_POST['hour'] - 12).':'.$_POST['minute'].':'.$_POST['second'].' PM ,'.$_POST['date'].'/'.$_POST['month'].'/'.$_POST['year'].'<br><br>';
            }else{
                print $_POST['hour'].':'.$_POST['minute'].':'.$_POST['second'].' AM ,'.$_POST['date'].'/'.$_POST['month'].'/'.$_POST['year'].'<br><br>';
            }
    
            switch($_POST['month']){
                case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                case 12:
                    print 'This month has 31 days';
                    break;
                case 2:
                    if($_POST['year']%4 == 0){
                        if($_POST['year']%100 == 0 && $_POST['year']%400 == 0){
                            print 'This month has 29 days';
                        }else{
                            print 'This month has 28 days';
                        }
                    }else{
                        print 'This month has 28 days';
                    }
                    break;
                case 4:
                case 6:
                case 9:
                case 11:
                    print 'This month has 30 days';
                    break;
                default:
                    break;
            }
        }      
    ?>


    <script>
        const month = document.getElementById('month');       
        const date = document.getElementById('date');
        const year = document.getElementById('year');
        let yearVal;
        year.onchange = function(){
            yearVal = parseInt(year.value);
            month.removeAttribute('disabled');
        }
        month.onchange = function(){
            let dateVal;
            var monthVal = parseInt(this.value);
            date.removeAttribute('disabled');

            switch(monthVal){
                case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                case 12:
                    dateVal = 31;
                    break;
                case 2:
                    if(yearVal%4 == 0){
                        if(yearVal%100 == 0 && yearVal%400 == 0){
                            dateVal = 29;
                        }else{
                            dateVal = 28;
                        }
                    }else{
                        dateVal = 28;
                    }
                    break;
                case 4:
                case 6:
                case 9:
                case 11:
                    dateVal = 30;
                    break;
            }

            let s;
            for(var i=0;i<=dateVal;i++)
            {
                if(i == 0){
                    s += '<option value="0">Chọn ngày</option>';
                }else{
                    s += '<option value="'+i+'">'+i+'</option>';
                }              
            }

            date.innerHTML = s;
        }

        console.log(yearVal);
    </script>
</body>
</html>